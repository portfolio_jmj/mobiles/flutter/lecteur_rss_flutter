# rssx

## Documentation Flutter

le fichier **pubspec.yaml** regroupe toutes les dépendances et les assets de l'application
le dossier **lib** comporte notre code
Le fichier **main.dart** est le point de démarrage de l'application
    c'est içi qu'on peux définir nos routes 
    ```dart
      initialRoute: '/',
      routes: {
        '/': (context) => ConfPage(),
        '/ConfPage': (context) => ConfPage(),
        '/ListOfFeed' : (context) => ListOfFeed()
      },
    ```

    les imports ce font en haut du fichier comme ceci `import 'package:rssx/services HiveService.dart';`

   ## Passage de paramètre route
   
    Pour passer des paramètres à une route on peux faire comme ceci : `Navigator.pushNamed(context, '/ListOfFeed', arguments: <String, String>{"user": "user01"});`

    ou créer une classe permettant de typer l'objet que nous allons passer à la clef arguments 

    ```dart
    class ConfPageArguments{
  
        final String user;

        ConfPageArguments(this.user);
    }
    ```

    cet qui nous donne `Navigator.pushNamed(context, '/ListOfFeed', arguments: ConfPageArguments("user01")); `

    Pour récupérer des paramètes de la route actuel on fait : `final ConfPageArguments params = ModalRoute.of(context).settings.arguments;` 


## Layout
GridView() pour afficher une liste de widget (card)
  - https://api.flutter.dev/flutter/widgets/GridView-class.html

hero() pour faire appraitre la card dans une page ex sur une image pas sur que ça fonctionne sur une card
 - https://www.youtube.com/watch?v=Be9UH1kXFDw&list=PLjxrf2q8roU23XGwz3Km7sQZFTdB996iG&index=18
 - https://api.flutter.dev/flutter/widgets/Hero-class.html

ToolTip() sur les iconce en bas de la card
 - https://www.youtube.com/watch?v=EeEfD5fI-5Q&list=PLjxrf2q8roU23XGwz3Km7sQZFTdB996iG&index=20
 - https://api.flutter.dev/flutter/material/Tooltip-class.html

 LayerOutBuilder() fonction builder argument contraints donne la dimension et permet de renvoyer un layout différent si besoin. un peu compliqué à appréhender. Car à force on commence par quoi !!
- https://www.youtube.com/watch?v=IYDVcriKjsw&list=PLjxrf2q8roU23XGwz3Km7sQZFTdB996iG&index=22

MediaQuery() mieux que le LayerOut

FittedBox() peux être qu'il faut que je construire ma card avec deux box une pour image en haut à droite 
au dessus d'une plus grande ?
- https://api.flutter.dev/flutter/widgets/FittedBox-class.html

Align()
- https://api.flutter.dev/flutter/widgets/Align-class.html

SizeBox() pour wraper n'importe quel element et le mettre à la dimension que l'on souhaite

LimitedBox permet qu'un enfant ne déborde pas comme wrap

ValueListenableBuilder et ValueNotifier pour gérer le state d'une variable dans plusieurs widget

AnimatedIcon