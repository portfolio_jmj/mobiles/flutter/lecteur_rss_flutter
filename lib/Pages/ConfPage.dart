import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:rssx/models/ConfPageArguments.dart';
import 'package:rssx/models/KeysEnum.dart';
import 'package:rssx/services/HiveService.dart';

class ConfPage extends StatefulWidget {
  @override
  _CatState createState() => _CatState();
}

class _CatState extends State<ConfPage> {

    bool _isVisibleUrl = true;
    bool _isVisibleCat = false;
    final _formKey = GlobalKey<FormState>();
    // Pour gérer l'input lui même
    final myController = TextEditingController();
    

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Visibility(
                visible: _isVisibleUrl,
                child: Padding(
                  padding: EdgeInsets.all(30.0),
                  child: TextFormField(
                    controller: myController,
                    decoration: const InputDecoration(
                      hintText: 'https://blog.eleven-labs.com/feed.xml',
                      labelText: 'Lien du flux Rss',
                    ),
                    style: TextStyle(fontSize: 24, height: 2.5),
                    // onFieldSubmitted: (String val) { RssService.RssService.getRss(val);}
                  ),
                )
              ),
              Visibility(
                visible: _isVisibleUrl,
                child: Container(
                  margin: EdgeInsetsDirectional.only(top: 30.0),
                  child: CupertinoButton.filled(
                    child: Text('valider'),
                    onPressed: () {
                      // RssService.RssService.getRss(myController.text);
                      List<String> listData;
                      listData.add('https://blog.eleven-labs.com/feed.xml');
                      HiveService.put('user01', KeyEnum.urls, listData)
                      .then((int result) {
                        print('ConfPage ' + result.toString());

                        assert(result == -1, 'Il y a eu une erreur');
                        if (result == -1)
                          print('error');
                        
                        Navigator.pushNamed(context, '/ListOfFeed', arguments: ConfPageArguments("user01"));
                      });

                      // setState(() {
                      //   _isVisibleUrl = !_isVisibleUrl;
                      //   _isVisibleCat = !_isVisibleCat;
                      // });
                      // print(_isVisibleCat);
                    }),
              )),
              Visibility(
                visible: _isVisibleCat,
                child: Padding(
                  padding: EdgeInsets.all(30.0),
                  child: TextFormField(
                    decoration: const InputDecoration(
                      hintText: 'dev front',
                      labelText: 'nom de la categorie',
                    ),
                    style: TextStyle(fontSize: 24, height: 2.5),
                  ),
                ),
              ),
              Visibility(
                visible: _isVisibleCat,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                        child: GestureDetector(onTap: () {
                          print('1');
                        }),
                        width: 40,
                        height: 20,
                        color: Color.fromRGBO(255, 0, 255, 1)),
                    Container(
                        child: GestureDetector(onTap: () {
                          print('2');
                        }),
                        width: 40,
                        height: 20,
                        color: Color.fromRGBO(153, 0, 255, 1)),
                    Container(
                        child: GestureDetector(onTap: () {
                          print('3');
                        }),
                        width: 40,
                        height: 20,
                        color: Color.fromRGBO(43, 120, 228, 1)),
                    Container(
                        child: GestureDetector(onTap: () {
                          print('4');
                        }),
                        width: 40,
                        height: 20,
                        color: Color.fromRGBO(0, 255, 255, 1)),
                    Container(
                        child: GestureDetector(onTap: () {
                          print('5');
                        }),
                        width: 40,
                        height: 20,
                        color: Color.fromRGBO(0, 158, 15, 1)),
                    Container(
                        child: GestureDetector(onTap: () {
                          print('6');
                        }),
                        width: 40,
                        height: 20,
                        color: Color.fromRGBO(255, 153, 0, 1)),
                    Container(
                        child: GestureDetector(onTap: () {
                          print('7');
                        }),
                        width: 40,
                        height: 20,
                        color: Color.fromRGBO(207, 42, 39, 1))
                  ],
                ),
              ),
              Visibility(
                visible: _isVisibleCat,
                child: Container(
                  margin: EdgeInsetsDirectional.only(top: 30.0),
                  child: CupertinoButton.filled(
                    child: Text('valider'),
                    onPressed: () {
                      print('pressed');
                    }),
              ))
            ]),
      ),
    );
  }
}
