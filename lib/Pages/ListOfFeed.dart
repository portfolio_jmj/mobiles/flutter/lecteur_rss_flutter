import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:rssx/components/CardRssItem.dart';
import 'package:webfeed/webfeed.dart';

class ListOfFeed extends StatefulWidget {
  @override
  _ListOfFeedState createState() => _ListOfFeedState();
}

class _ListOfFeedState extends State<ListOfFeed> {
  String url = 'https://blog.eleven-labs.com/feed.xml';
  String feedName;
  String feedUrl;
  //String fluxAuthor;
  Future<List<RssItem>> getRss(String url) async {
    print("RssService getRss");
    try {
      var data = await http.get(url);
      String body = utf8.decode(data.bodyBytes);;
      RssFeed feed = RssFeed.parse(body);
      this.feedName = feed.title;
      this.feedUrl = feed.link;
      return feed.items;
    } catch (e) {
      print(e);
      return new RssFeed().items;
    }
  }

  @override
  // https://www.youtube.com/watch?v=EwHMSxSWIvQ
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
            child: FutureBuilder(
                future: getRss(url),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  if (snapshot.data == null) {
                    print(snapshot.data);
                    return Container(
                      child: Center(child: Text("Loading ...")),
                    );
                  } else {
                    return ListView.builder(
                        itemCount: snapshot.data.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Column(children: [
                            Container(
                              padding: EdgeInsets.all(25.0),
                              child: CardRssItem(rssItem: snapshot.data[index], feedName: this.feedName, feedUrl: this.feedUrl),
                            )
                          ]);
                        });
                  }
                })));
  }
}