import 'package:flutter/material.dart';
import 'package:rssx/models/CustColor.dart';

class CardActionButton extends StatefulWidget {
  final CustColor custColor;
  const CardActionButton({Key key, @required this.custColor}) : super(key: key);

  @override
  _CardActionButtonState createState() => _CardActionButtonState();
}

class _CardActionButtonState extends State<CardActionButton> {
  Color get custColor => this.widget.custColor.custColor;
  double _buttonWidth = 32.0;
  BoxShape _boxShape = BoxShape.circle;
  BorderRadius _borderRadius;
  bool _toggleTriger = true;
  static Color _colorChildButtons = Colors.white;
  static double _sizeChildButtons = 32.0;
  Row _childs;
  @override
  Widget build(BuildContext context) {
    setState(() {
    _childs = Row( children: [ 
      GestureDetector(child: Icon( Icons.info_outline, color: _colorChildButtons, size: _sizeChildButtons), onTap: () => _toggleState(),)]);  
    });
    return Container(
      child: Container(
        height: 32,
        width: _buttonWidth,
        child: _childs,
        decoration: BoxDecoration(
          shape: _boxShape,
          borderRadius: _borderRadius,
          color: custColor,
        ),
      ),
    );
  }

  void _toggleState() {
    setState(() {
      _toggleTriger = !_toggleTriger;  
    });
    if(_toggleTriger) {
      setState(() {
      _buttonWidth = 32.0;
      _boxShape = BoxShape.circle;
      _borderRadius = null;
      _childs = Row(children :[GestureDetector(child: Icon( Icons.info_outline, color: _colorChildButtons, size: _sizeChildButtons), onTap: () => _toggleState(),)]);
    });
    print(_childs.children.length);
    } else{
      setState(() {
        _buttonWidth = 128.0;
        _boxShape = BoxShape.rectangle;
        _borderRadius = BorderRadius.circular(5);
        _childs = Row( mainAxisAlignment: MainAxisAlignment.spaceAround ,
          children:[
            GestureDetector(child: Icon( Icons.visibility_off, color: Colors.black, size: _sizeChildButtons), onTap: () => print("shadow")),
            GestureDetector(child: Icon( Icons.save, color: _colorChildButtons, size: _sizeChildButtons), onTap: () => print("save"),),
            GestureDetector(child: Icon( Icons.indeterminate_check_box, color: _colorChildButtons, size: _sizeChildButtons), onTap: () => _toggleState(),)
          ]);
      });
      print(_childs.children.length);
    }
  }
}
