import 'package:flutter/material.dart';
import 'package:rssx/components/CardActionButton.dart';
import 'package:rssx/models/CustColor.dart';
import 'package:webfeed/domain/rss_item.dart';

import 'package:html/parser.dart' show parse;


import 'ViewContent.dart';


class CardRssItem extends StatefulWidget {
  final RssItem rssItem;
  final String feedName;
  final String feedUrl;
  CardRssItem({Key key, @required this.rssItem, @required this.feedName, @required this.feedUrl}) : super(key: key);
  

  @override
  _CardRssItemState createState() => _CardRssItemState();
}

class _CardRssItemState extends State<CardRssItem> {
  RssItem get rssItem => this.widget.rssItem;
  String get feedName => this.widget.feedName;
  String description ='';
  @override
  Widget build(BuildContext context) {
    
    var doc = parse(rssItem?.description);
    String img;
    try{
      String src = doc.querySelector('img').attributes.entries.first.value;
      //String img = rssItem.link.substring(0,(rssItem.link.length) -28)+doc.querySelector('img').attributes.entries.first.value;
      //print(src);
      if(src.startsWith('http')){
        img=src;
      }
      else{
        img = this.widget.feedUrl.substring(0, this.widget.feedUrl.length - 1) + src;
      }
    }
    catch(e){
      img = 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/1024px-No_image_available.svg.png';
    }
      
    
    
    //print(rssItem.link);
    
    for (var x in doc.children) {
      description += x.text.replaceAll('\n', ' '); 
    }

    return  _buildCustomCard(
      (rssItem?.pubDate).toString().substring(5,16),
      rssItem?.author,
      feedName,
      rssItem?.title,
      description.trim(),
      rssItem?.link,
      Color.fromRGBO(255, 159, 155, 1),
      img,
      context);
    }
  }

Stack _buildCustomCard(String date, 
    String author,
    String feedName,
    String title,
    String description,
    String link,
    Color custColor, 
    String img,
    BuildContext context) {
  return Stack(
    overflow: Overflow.visible,
    children: <Widget>[
      Container(
        padding: EdgeInsets.only(top: 10),
        width: 340,
        height: 200,
        child: Stack(
          children: <Widget>[
            Positioned(
              left: 120,
              child: Text((date != null)? date : "" ,
                style:
                  TextStyle(fontSize: 15, fontWeight: FontWeight.w600)
                ),
            ),
            /*Positioned(
              right: 30,
              child: Text((author != null)? author : feed,
                style:
                  TextStyle(fontSize: 15, fontWeight: FontWeight.w600)
                ),
            ),*/
            Positioned(
              top: 35,
              left: 5,
              width: 330,
              child: GestureDetector(
                child:Text((title != null)? title : "",
                //textAlign: TextAlign.center,
                overflow: TextOverflow.fade,
                maxLines: 2,
                style: TextStyle(
                  color: custColor,
                  fontSize: 17,
                  fontWeight: FontWeight.w900
                )
              ),
              onTap: () async {
                Navigator.push(context, MaterialPageRoute(
                  builder: (context) => ViewContent(link : link)));
              } ,
              )
              
            ),
            Positioned(
              top: 75,
              left: 10,
              width: 300,
              child: Text(
                description.substring(0,300).padRight(325,'...'),
                softWrap: true,
                maxLines: 4,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      fontSize: 13, fontWeight: FontWeight.w500, height: 1.3),
                ),
              ),
            ],
          ),
          decoration: BoxDecoration(
            color: Colors.grey[100],
            borderRadius: BorderRadius.circular(8),
            boxShadow: [
              BoxShadow(
                color: Colors.grey[600],
                blurRadius: 6,
                offset: Offset(4, 4)
              ),
            ]
          ),
        ),
        Positioned(
          top: -30,
          left: 25,
          child: Container(
            width: 60,
            height: 60,
            decoration: BoxDecoration(
              color: Colors.indigo,
              shape: BoxShape.circle,
              border: Border.all(color: custColor, width: 2),
              image: DecorationImage(
                  fit: BoxFit.fill,
                  //image: AssetImage('images/Pokemon-Go-PokeBalls.jpg')
                  image : NetworkImage(img)
              )
            ),
          ),
        ),
        Positioned(
          bottom: 5,
          right: 5,
          child: CardActionButton(custColor: new CustColor(custColor))
        ),
      ],
    );
  }
