import 'dart:async';

import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class ViewContent extends StatefulWidget {
  final String link;
  ViewContent({Key key, @required this.link}) : super(key: key);

  @override
  _ViewContentState createState() => _ViewContentState();
}

class _ViewContentState extends State<ViewContent> {
  String get link => this.widget.link;
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       //appBar: AppBar(),
       body: Column(
         children :[
           Expanded(child: WebView(
            initialUrl: link,
            javascriptMode: JavascriptMode.disabled,
            onWebViewCreated: (WebViewController webViewController) {
            _controller.complete(webViewController);
          },

           ),)
         ]
       ),
    );
  }
}