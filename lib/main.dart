import 'package:flutter/material.dart';
import 'package:rssx/pages/ListOfFeed.dart';

void main() => {
  runApp(MyApp())
  };

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Rss',
      theme: ThemeData(
        primarySwatch: Colors.purple,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => ListOfFeed(),
      },
    );
  }
}