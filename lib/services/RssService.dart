import 'package:http/http.dart' as http;
import 'package:rssx/services/HiveService.dart';
import 'package:webfeed/webfeed.dart';

import 'dart:async';

// refaire en mode factoty pour avoir un vrai class abstraite
 class RssService {

    /// récupérer le flux via un appel http prenant une url en params
    static Future<RssFeed> getRss(String url) async {
        
        print("RssService getRss");
        try{
          String data = await http.read(url);
          RssFeed feed = parseRss(data);
          return feed;
        }  
        catch(e){
          print(e);
          return new RssFeed();
        }
      }

    // recupérer plusieurs flux via une liste d'url passé en params
      static List<RssFeed> getMultiRss(List<String> urls) {
        
        List<RssFeed> listFeed;
        urls.forEach( (String item) async {
          RssFeed feed = await getRss(item);
          listFeed.add(feed);
        });
        
        return listFeed;
      } 

    /// Parser les données brutes en json
      static RssFeed parseRss(String data) {
        try{
        return RssFeed.parse(data);        
        }
        catch(e){
          print(e);
          return new RssFeed();
        }
      }

  
  
}